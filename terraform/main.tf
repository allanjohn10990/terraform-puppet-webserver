provider "aws" {
  access_key = ""
  secret_key = ""
  region     = "eu-west-1"
}

# Variables for VPC module
module "vpc" {
	source = "./modules/vpc"
  tag = "${var.tag}"
	enable_dns_support = true
	enable_dns_hostnames = true
  # jumphost_sg_id = "${module.jumphost_sg.jumphost_sg_id}"
	vpc_cidr = "10.100.0.0/16"
        public_subnets_cidr = "10.100.10.0/24,10.100.20.0/24"
        private_subnets_cidr = "10.100.30.0/24,10.100.40.0/24"
        azs    = "eu-west-1a,eu-west-1b"
}

module "jumphost_sg" {
	source = "./modules/jumphost_sg"
  tag = "${var.tag}"
	vpc_id = "${module.vpc.vpc_id}"
	source_cidr_block = "0.0.0.0/0"
}

module "webserver_sg" {
	source = "./modules/webserver_sg"
  tag = "${var.tag}"
	vpc_id = "${module.vpc.vpc_id}"
  source_cidr_block = "10.100.10.0/24,10.100.20.0/24"
  security_group_id = "${module.jumphost_sg.jumphost_sg_id}"
}

module "elb_sg" {
	source = "./modules/elb_sg"
  tag = "${var.tag}"
	vpc_id = "${module.vpc.vpc_id}"
  source_cidr_block = "0.0.0.0/0"
}

module "puppetmaster_sg" {
	source = "./modules/puppetmaster_sg"
  tag = "${var.tag}"
	vpc_id = "${module.vpc.vpc_id}"
  database_sg = "${module.database_sg.database_sg_id}"
  puppet_configure = "${module.webserver_sg.webserver_sg_id}"
  security_group_id = "${module.jumphost_sg.jumphost_sg_id}"
}

module "database_sg" {
	source = "./modules/database_sg"
  tag = "${var.tag}"
	vpc_id = "${module.vpc.vpc_id}"
  ws_security_group = "${module.webserver_sg.webserver_sg_id}"
  security_group_id = "${module.jumphost_sg.jumphost_sg_id}"
}

# module "rds_sg" {
#     source = "./modules/rds_sg"
#     tag = "${var.tag}"
#     vpc_id = "${module.vpc.vpc_id}"
#     security_group_id = "${module.webserver_sg.webserver_sg_id}"
# }


module "key" {
	source = "./modules/key"
	key_name = "olxtest"
	public_key = "${file("olxtest.pub")}"
}

# module "webserver" {
# 	source = "./modules/webserver"
#   tag = "${var.tag}"
#   count = "2"
# 	ami_id = "ami-58d7e821"
# 	key_name = "${module.key.ec2key_name}"
# 	security_group_id = "${module.webserver_sg.webserver_sg_id}"
# 	subnet_id = "${module.vpc.public_subnets_id}"
# 	instance_type = "t2.micro"
#   user_data = "#!/bin/bash\napt-get -y update\nsudo apt-get install apache2 -y\nsudo service apache2 start"
# }
#
# #
#
module "database" {
	source = "./modules/database"
  tag = "${var.tag}"
  count = "2"
	ami_id = "ami-58d7e821"
	key_name = "${module.key.ec2key_name}"
	security_group_id = "${module.database_sg.database_sg_id}"
	subnet_id = "${module.vpc.private_subnets_id}"
	instance_type = "t2.micro"
  puppetmaster_ip = "${module.puppetmaster.puppetmaster_ip}"
  # user_data = "#!/bin/bash\napt-get -y update\nsudo apt-get install apache2 -y\nsudo service apache2 start"
}

module "puppetmaster" {
	source = "./modules/puppetmaster"
  tag = "${var.tag}"
	ami_id = "ami-58d7e821"
	key_name = "${module.key.ec2key_name}"
	security_group_id = "${module.puppetmaster_sg.puppet_sg_id}"
	subnet_id = "${module.vpc.public_subnets_id}"
	instance_type = "t2.medium"
  # user_data = "#!/bin/bash\napt-get -y update\nsudo apt-get install apache2 -y\nsudo service apache2 start"
}

module "jumphost" {
	source = "./modules/jumphost"
  tag = "${var.tag}"
	ami_id = "ami-58d7e821"
	key_name = "${module.key.ec2key_name}"
	security_group_id = "${module.jumphost_sg.jumphost_sg_id}"
	subnet_id = "${module.vpc.public_subnets_id}"
	instance_type = "t2.nano"
 }
#
#
# module "rds" {
# 	source = "./modules/rds"
#   tag = "${var.tag}"
# 	storage = "10"
# 	engine_version = "5.6.27"
#   multi_az = true
# 	db_name = "${var.rds_db}"
# 	username = "${var.rds_user}"
# 	password = "${var.rds_password}"
# 	security_group_id = "${module.rds_sg.rds_sg_id}"
# 	subnet_ids = "${module.vpc.private_subnets_id}"
# }

# module "route53" {
#   source = "./modules/route53"
#   name = "sretestdb.olxtest.com"
#   records = "${module.rds.rds_address}"
#   vpc_id = "${module.vpc.vpc_id}"
# }
#
# module "s3" {
#   source = "./modules/s3"
#   bucketname = "ansiblehost-olxtest"
# }

module "elb" {
	source = "./modules/elb"
  tag = "${var.tag}"
	security_groups = "${module.elb_sg.elb_sg_id}"
	availability_zones = "eu-west-1a,eu-west-1b"
	subnets = "${module.vpc.public_subnets_id}"
	# instance_id = "${module.webserver.webserver_id}"
}

module "lc" {
  source = "./modules/launch_config"
  tag = "${var.tag}"
  ami_id = "ami-58d7e821"
  instance_type = "t2.micro"
  security_group_id = "${module.webserver_sg.webserver_sg_id}"
  key_name = "${module.key.ec2key_name}"
  puppetmaster_ip = "${module.puppetmaster.puppetmaster_ip}"
  # user_data = "#!/bin/bash\napt-get -y update\nsudo apt-get install apache2 -y\nsudo service apache2 start"
}

module "asg" {
  source = "./modules/asg"
  launch_configuration = "${module.lc.webserver_lc}"
  availability_zones = "eu-west-1a,eu-west-1b"
  zone_identifier = "${module.vpc.public_subnets_id}"
  lb = "${module.elb.elb_name}"
}
