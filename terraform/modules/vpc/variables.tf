variable "tag" {
	default = "test"
}
variable "enable_dns_support" {
  default = false
}
variable "enable_dns_hostnames" {
  default = false
}

variable "vpc_cidr" {
  default     = "10.100.0.0/16"
}

variable "public_subnets_cidr" {
	default = "10.100.10.0/24,10.100.20.0/24"
}

variable "private_subnets_cidr" {
	default = "10.100.30.0/24,10.100.40.0/24"
}

variable "azs" {
	default = "eu-west-1a,eu-west-1b"
}

variable "map_public_ip_on_launch" {
  default = true
}
