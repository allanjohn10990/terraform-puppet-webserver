resource "aws_s3_bucket" "s3" {
  bucket = "${var.bucketname}"
  acl    = "private"

  tags {
    sre_candidate = "${var.tag}"
  }
}
