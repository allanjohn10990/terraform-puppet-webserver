## Creating Launch Configuration
resource "aws_launch_configuration" "lc" {
  image_id  = "${var.ami_id}"
  instance_type = "${var.instance_type}"
  security_groups = ["${split(",", var.security_group_id)}"]
  key_name = "${var.key_name}"
  # user_data = "${var.user_data}"
  user_data = <<-EOF
            #!/bin/bash
            echo "${var.puppetmaster_ip} puppet" >> /etc/hosts
            cd ~ && wget https://apt.puppetlabs.com/puppetlabs-release-pc1-trusty.deb
            dpkg -i puppetlabs-release-pc1-trusty.deb
            apt-get update
            apt-get install puppet-common puppet -y
            /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
            sed -i '/\[main\]/a runinterval=30' /etc/puppet/puppet.conf
            service puppet restart
            puppet agent --enable
            puppet agent -t
            EOF
  lifecycle {
    create_before_destroy = true
  }
}

output "webserver_lc" {
    value = "${aws_launch_configuration.lc.id}"
}
