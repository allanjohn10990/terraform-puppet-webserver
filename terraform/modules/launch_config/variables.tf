// Module specific variables
variable "tag" {
	default = "test"
}

variable "ami_id" {
  default = "test"
}

variable "user_data" {
  default = "test"
}

variable "instance_type" {
  description = "t2.micro"
}

variable "key_name" {
	description = "test"
}

variable "puppetmaster_ip" {
	description = "test"
}

variable "security_group_id" {
	description = "sg-id"
}
