// Module specific variables
variable "tag" {
	default = "test"
}

variable "vpc_id" {
  description = "vpc id"
}

variable "security_group_id" {
  description = "sg id"
}
