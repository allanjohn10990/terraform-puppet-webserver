resource "aws_security_group" "jumphost" {
    vpc_id = "${var.vpc_id}"
    tags {
      name = "jh-sg"
      sre_candidate = "${var.tag}"
    }
    // allows traffic from the SG itself
    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        self = true
    }

    // allow traffic for TCP 22
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.source_cidr_block}"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

output "jumphost_sg_id" {
  value = "${aws_security_group.jumphost.id}"
}
