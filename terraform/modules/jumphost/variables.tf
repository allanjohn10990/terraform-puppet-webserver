// Module specific variables
variable "tag" {
  default = "test"
}
variable "server_role" {
  default = "test"
}

variable "instance_type" {
	description = "Type of instance"
	default = "t2.nano"
}

variable "key_name" {
	default = "deployment-201407"
}

variable "security_group_id" {
	description = "sg id"
}

variable "subnet_id" {
  description = "vpc id"
}

variable "ami_id" {
  description = "ami"
}
