resource "aws_instance" "jumphost" {
  ami = "${var.ami_id}"
  vpc_security_group_ids = ["${split(",", var.security_group_id)}"]
  key_name = "${var.key_name}"
  instance_type = "${var.instance_type}"
  subnet_id = "${element(split(",", var.subnet_id), count.index%2)}"
  tags {
    sre_candidate = "${var.tag}"
  }
}

output "jumphost_id" {
  value = "${aws_instance.jumphost.id}"
}
