resource "aws_instance" "database" {
  ami = "${var.ami_id}"
  count = "${var.count}"
  vpc_security_group_ids = ["${split(",", var.security_group_id)}"]
  key_name = "${var.key_name}"
  instance_type = "${var.instance_type}"
  subnet_id = "${element(split(",", var.subnet_id), count.index%2)}"
  user_data = <<-EOF
            #!/bin/bash
            echo "${var.puppetmaster_ip} puppet" >> /etc/hosts
            cd ~ && wget https://apt.puppetlabs.com/puppetlabs-release-pc1-trusty.deb
            dpkg -i puppetlabs-release-pc1-trusty.deb
            apt-get update
            apt-get install puppet-common puppet git -y
            /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
            sed -i '/\[main\]/a runinterval=30' /etc/puppet/puppet.conf
            service puppet restart
            puppet agent --enable
            puppet agent -t
            EOF
  tags {
    sre_candidate = "${var.tag}"
    Name = "database"
  }
}

output "database_id" {
  value = "${join(",", aws_instance.database.*.id)}"
}
