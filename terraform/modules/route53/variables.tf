variable "name" {
  default = "testing.example.com"
}

variable "records" {
  default = "sample record name"
}

variable "vpc_id" {
  default = "vpc"
}
