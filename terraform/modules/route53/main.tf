resource "aws_route53_zone" "selected" {
  name         = "olxtest.com"
  # private_zone = "true"
  vpc_id = "${var.vpc_id}"
}

resource "aws_route53_record" "database" {
  zone_id = "${aws_route53_zone.selected.zone_id}"
  name = "${var.name}"
  type = "CNAME"
  ttl = "30"
  records = ["${var.records}"]
}
