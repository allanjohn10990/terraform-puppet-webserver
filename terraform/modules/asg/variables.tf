// Module specific variables
variable "tag" {
	default = "test"
}

variable "launch_configuration" {
  default = "test"
}

variable "zone_identifier" {
  default = "test"
}

variable "availability_zones" {
  description = "az"
}

variable "lb" {
	description = "elb-dns-name"
}
