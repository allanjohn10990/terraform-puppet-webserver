## Creating AutoScaling Group
resource "aws_autoscaling_group" "asg" {
  launch_configuration = "${var.launch_configuration}"
  availability_zones = ["${split(",", var.availability_zones)}"]
  vpc_zone_identifier = ["${split(",", var.zone_identifier)}"]
  min_size = 2
  max_size = 3
  desired_capacity = 2
  load_balancers = ["${var.lb}"]
  health_check_type = "ELB"
  tags = [
  {
    key = "sre_candidate"
    value = "allan_john"
    propagate_at_launch = true
  },
  {
    key = "Name"
    value = "webserver"
    propagate_at_launch = true
  },
]
}

resource "aws_autoscaling_policy" "scaleup" {
  name = "scaleup policy"
  scaling_adjustment = 1
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = "${aws_autoscaling_group.asg.name}"
}

resource "aws_autoscaling_policy" "autopolicy-down" {
  name = "scaledown policy"
  scaling_adjustment = -1
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = "${aws_autoscaling_group.asg.name}"
}
