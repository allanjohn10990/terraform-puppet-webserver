resource "aws_security_group" "puppetmaster_sg" {
    vpc_id = "${var.vpc_id}"
    tags {
      name = "pm-sg"
      sre_candidate = "${var.tag}"
    }
    // allow traffic for TCP 80
    ingress {
        from_port = 8140
        to_port = 8140
        protocol = "tcp"
        security_groups = ["${var.puppet_configure}","${var.security_group_id}","${var.database_sg}"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        security_groups = ["${var.security_group_id}"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

output "puppet_sg_id" {
  value = "${aws_security_group.puppetmaster_sg.id}"
}
