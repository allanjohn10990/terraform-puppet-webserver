// Module specific variables
variable "tag" {
	default = "test"
}

variable "vpc_id" {
  description = "vpc id"
}

variable "security_group_id" {
  description = "sg id"
}

variable "database_sg" {
  description = "sg id"
}

variable "puppet_configure" {
  description = "sg id"
}
