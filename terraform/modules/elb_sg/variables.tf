// Module specific variables
variable "tag" {
	default = "test"
}

variable "vpc_id" {
  description = "vpc id"
}

variable "source_cidr_block" {
  default = "0.0.0.0/0"
}
