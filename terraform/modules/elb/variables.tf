// Module specific variables
variable "tag" {
	default = "test"
}
variable "availability_zones" {
  description = "az"
}

# variable "instance_id" {
# 	description = "instance id"
# }

variable "subnets" {
  description = "Subnets for ELB"
}

variable "security_groups" {
  description = "sg for elb"
}

variable "source_cidr_block" {
  default = "0.0.0.0/0"
}
