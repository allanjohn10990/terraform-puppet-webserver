resource "aws_elb" "elb" {
    security_groups = ["${var.security_groups}"]
    subnets = ["${split(",", var.subnets)}"]
    tags {
      name = "elb"
      sre_candidate = "${var.tag}"
    }

    listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
    }

    listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 443
    lb_protocol = "https"
    ssl_certificate_id = "arn:aws:acm:eu-west-1:060948813048:certificate/86c78ff8-22c6-42f1-8fe3-dd57a6ac6d3d"
    }

    health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "TCP:80"
    interval = 30
    }

    cross_zone_load_balancing = true
    idle_timeout = 400
    connection_draining = true
    connection_draining_timeout = 400

    provisioner "local-exec" {
    command = "echo ELB_DNS_NAME:${aws_elb.elb.dns_name} >> data.yml"
    }
}

output "elb_dns_name" {
  value = "${aws_elb.elb.dns_name}"
}

output "elb_name" {
  value = "${aws_elb.elb.name}"
}
