// Module specific variables
variable "tag" {
	default = "test"
}

variable "vpc_id" {
  description = "vpc id"
}

variable "ws_security_group" {
  description = "ws_security_group"
}

variable "security_group_id" {
  default = "sg of jumphost"
}
