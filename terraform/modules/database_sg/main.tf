resource "aws_security_group" "database_sg" {
    vpc_id = "${var.vpc_id}"
    tags {
      name = "db-sg"
      sre_candidate = "${var.tag}"
    }
    // allow traffic for TCP 80
    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        security_groups = ["${var.ws_security_group}"]

    }

    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        self = true
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        security_groups = ["${var.security_group_id}"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

output "database_sg_id" {
  value = "${aws_security_group.database_sg.id}"
}
