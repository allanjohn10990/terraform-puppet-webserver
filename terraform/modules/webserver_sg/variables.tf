// Module specific variables
variable "tag" {
	default = "test"
}

variable "vpc_id" {
  description = "vpc id"
}

variable "source_cidr_block" {
  description = "cidr-public"
}

variable "security_group_id" {
  default = "sg of jumphost"
}
