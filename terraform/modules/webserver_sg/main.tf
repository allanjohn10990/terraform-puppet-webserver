resource "aws_security_group" "webserver_sg" {
    vpc_id = "${var.vpc_id}"
    tags {
      name = "ws-sg"
      sre_candidate = "${var.tag}"
    }
    // allow traffic for TCP 80
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["${split(",", var.source_cidr_block)}"]
    }
    // allow traffic for TCP 443
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["${split(",", var.source_cidr_block)}"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        security_groups = ["${var.security_group_id}"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

output "webserver_sg_id" {
  value = "${aws_security_group.webserver_sg.id}"
}
