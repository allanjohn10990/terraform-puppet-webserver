resource "aws_instance" "webserver" {
  ami = "${var.ami_id}"
  count = "${var.count}"
  vpc_security_group_ids = ["${split(",", var.security_group_id)}"]
  key_name = "${var.key_name}"
  instance_type = "${var.instance_type}"
  subnet_id = "${element(split(",", var.subnet_id), count.index%2)}"
  user_data = "${var.user_data}"
  tags {
    sre_candidate = "${var.tag}"
    Name = "webserver"
  }
}

output "webserver_id" {
  value = "${join(",", aws_instance.webserver.*.id)}"
}
