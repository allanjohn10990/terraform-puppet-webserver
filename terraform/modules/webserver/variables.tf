variable "tag" {
  default = "test"
}

variable "count" {
  description = "number of instances"
}

variable "instance_type" {
	description = "Type of instance"
	default = "t2.nano"
}

variable "key_name" {
	default = "deployment-201407"
}

variable "user_data" {
	description = "command to run in instance"
}

variable "security_group_id" {
	description = "sg id"
}

variable "subnet_id" {
  description = "vpc id"
}

variable "ami_id" {
  description = "ami"
}
