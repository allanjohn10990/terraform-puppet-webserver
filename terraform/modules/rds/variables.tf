// Module specific variables
variable "tag" {
  default = "test"
}

variable "subnet_ids" {
  description = "Subnet id(s) for ELB"
}

variable "identifier" {
  default = "test-rds"
  description = "name for instance"
}

variable "security_group_id" {
  description = "sg for rds"
}

variable "storage" {
  default = "10"
}

variable "engine" {
  default = "mysql"
}

variable "engine_version" {
  default = "5.6.22"
}

variable "multi_az" {
  default = false
}

variable "instance_class" {
  default = "db.t2.micro"
}

variable "db_name" {
  default = "testdb"
}

variable "username" {
  default = "test"
}

variable "password" {
  description = "passwords"
}
