resource "aws_db_subnet_group" "rds" {
  subnet_ids = ["${split(",", var.subnet_ids)}"]
  tags {
    sre_candidate = "${var.tag}"
  }
}

resource "aws_db_instance" "rds" {
  identifier = "webserver"
  allocated_storage = "${var.storage}"
  engine = "${var.engine}"
  engine_version = "${var.engine_version}"
  instance_class = "${var.instance_class}"
  multi_az = "${var.multi_az}"
  name = "${var.db_name}"
  username = "${var.username}"
  password = "${var.password}"
  db_subnet_group_name = "${aws_db_subnet_group.rds.id}"
  vpc_security_group_ids = ["${var.security_group_id}"]
  tags {
    sre_candidate = "${var.tag}"
  }
  provisioner "local-exec" {
  command = "echo RDS_NAME:${aws_db_instance.rds.address} >> data.yml"
  }
}

output "rds_address" {
  value = "${aws_db_instance.rds.address}"
}
