resource "aws_instance" "puppetmaster" {
  ami = "${var.ami_id}"
  vpc_security_group_ids = ["${split(",", var.security_group_id)}"]
  key_name = "${var.key_name}"
  instance_type = "${var.instance_type}"
  subnet_id = "${element(split(",", var.subnet_id), count.index%2)}"
  user_data = <<-EOF
            #!/bin/bash
            cd /opt
            wget https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
            dpkg -i puppetlabs-release-pc1-xenial.deb
            apt update
            apt install -y puppetmaster-passenger git
            sed -i '/\[main\]/a autosign=true' /etc/puppet/puppet.conf
            sed -i '/\[main\]/a dns_alt_names=puppet' /etc/puppet/puppet.conf
            echo "*.eu-west-1.compute.internal" > /etc/puppet/autosign.conf
            service puppetqd restart
            service apache2 restart
            git clone "https://github.com/lovelinuxalot/puppet-test"
            cp -r puppet-test/modules /etc/puppet/
            cp -r puppet-test/manifests /etc/puppet/
            EOF
  tags {
    sre_candidate = "${var.tag}"
    Name = "puppetmaster"
  }
}

output "puppetmaster_ip" {
  value = "${aws_instance.puppetmaster.private_ip}"
}
